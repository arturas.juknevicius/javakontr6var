﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ats
{
    class Flight
    {
        private     static      double     USD_PRICE = 0.86;
        //skrydžio kodą, lėktuvo tipą, išvykimo tašką, atvykimo tašką, skrydžio trukmę minutėmis, skrydžio kainą
        private     string      _flightCode;
        private     string      _planeType;
        private     string      _origin;
        private     string      _dest;
        private     int         _duration;
        private     double      _priceEur;

        public string FlightCode { get => _flightCode; set => _flightCode = value; }
        public string PlaneType { get => _planeType; set => _planeType = value; }
        public string Origin { get => _origin; set => _origin = value; }
        public string Dest { get => _dest; set => _dest = value; }
        public int Duration { get => _duration; set => _duration = value; }
        public double PriceEur { get => _priceEur; set => _priceEur = value; }

        public double PriceUSD() { return this._priceEur * Flight.USD_PRICE; }

        public Flight(string flightCode, string planeType)
        {
            _flightCode = flightCode;
            _planeType = planeType;
        }

        public Flight(string flightCode, string planeType, string origin, string dest, int duration, double price)
        {
            _flightCode = flightCode;
            _planeType = planeType;
            _origin = origin;
            _dest = dest;
            _duration = duration;
            _priceEur = price;
        }

        public static Flight Parse(string line)
        {
            line = line.Replace('.', ',')
                       .Replace(" - ", ";")
                       .Replace(':', ';')
                       .Replace(", ", ";");

            var columns = line.Split(';');

            return new Flight(columns[2],
                              columns[3],
                              columns[0], 
                              columns[1], 
                              int.Parse(columns[5].TrimEnd(',')), 
                              double.Parse(columns[4].Replace('.', ','))
                              );
        }

        // {KODAS}: {išvykimas} – {atvykimas}, {trukmė, valandos:minutes}, {kaina, eurais ir doleriais},{lektuvo tipo pirmi 4 simboliai
        public override string ToString()
        {
            string plane = new string(this._planeType.Take(4).ToArray());
            return this._flightCode + ": " +
                   this._origin + " - " + this._dest + ", " +
                   ((this._duration / 60) + ":" + (this._duration % 60)) + ", " +
                   (this._priceEur + " EUR " + PriceUSD() + " USD ") + ", " +
                   plane;
        }
    }
}
