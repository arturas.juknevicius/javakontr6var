﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;

namespace ats
{
    class Program
    {
        private static List<Flight> ProcessFile(string path)
        {
            List<Flight> Flights = new List<Flight>();

            string OriginDest = "";
            foreach (var line in File.ReadAllLines(path))
            {
                if (':' == line.Last())
                    OriginDest = line;
                else
                    Flights.Add(Flight.Parse(OriginDest + line));
            }

            return Flights;
        }

        public static void FilterToFile(ref List<Flight> Flights)
        {
            int prefDuration;
            while (true)
            {
                Console.WriteLine("Iveskite pageidaujama skrydzio trukme:");
                string input = Console.ReadLine();
                if (int.TryParse(input, out int i))
                {
                    prefDuration = i;
                    break;
                }
            }

            double prefPrice;
            while (true)
            {
                Console.WriteLine("Iveskite pageidaujama skrydzio kaina:");
                string input = Console.ReadLine();
                if (double.TryParse(input, out double d))
                {
                    prefPrice = d;
                    break;
                }
            }

            List<string> query = Flights.Where(flight => flight.Duration < prefDuration).Select(flight => flight.ToString()).ToList();
            File.WriteAllLines("ShorterFlights.txt", query);

            query = Flights.Where(flight => flight.PriceEur < prefPrice).Select(flight => flight.ToString()).ToList();
            File.WriteAllLines("CheaperFlights.txt", query);
        }

        static void Main(string[] args)
        {
            var Flights = ProcessFile("data.txt");

            Flights.ForEach(f => Console.WriteLine(f.ToString()));

            Flights = Flights.OrderBy(f => f.PlaneType).ThenBy(f => f.Duration).ThenBy(f => f.PriceEur).ToList();

            Console.WriteLine("Ordered:");
            Flights.ForEach(f => Console.WriteLine(f.ToString()));

            FilterToFile(ref Flights);

            Console.Read();
        }
    }
}
